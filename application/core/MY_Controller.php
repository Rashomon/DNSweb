<?php

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
//        $this->checkHasLogin();
    }

    public function assign($key, $val)
    {
        $this->ci_smarty->assign($key, $val);
    }

    public function display($html)
    {
        $this->ci_smarty->display($html);
    }

//    //检查用户是否登录
//    public function checkHasLogin(){
//        $hasLogin = isset($_SESSION['uid']);
//        if(!$hasLogin){
//            header("Location:/login/showLogin");
//        }
//    }


}
<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/9/4
 * Time: 上午10:36
 * config模型
 */
class Config_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @return array
     * 获取当前配置
     */
    public function getConfig(){
        $config = [];
        $sqlData = $this->db->select("name,value")->from("config")
            ->order_by("name")
            ->get()->result_array();
        foreach ($sqlData as $data){
            $config[$data['name']] = $data['value'];
        }
        return $config;
    }

    /**
     * @param $configs
     * @return array
     * 更新配置
     */
    public function updateConfig($configs){
        $this->db->trans_begin();
        foreach (array_keys($configs) as $key){
            $this->db->where('name',$key);
            $this->db->update('config',[
               'value'  =>  $configs[$key]
            ]);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return [
                'code'  => 0,
                'msg'   =>  '更新失败!'
            ];
        }
        else
        {
            $this->db->trans_commit();
            return [
                'code'  => 1,
                'msg'   =>  '更新成功!'
            ];
        }
    }



}
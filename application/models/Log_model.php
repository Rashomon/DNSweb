<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/9/4
 * Time: 下午2:12
 * 操作日志模型
 */
class Log_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param $page
     * @param $pageSize
     * @param $search
     * @param $selectNS
     * @param $selectDomain
     * @return array
     * 分页获取 操作日志
     */
    public function getLogInfo($page,$pageSize,$search,$selectNS,$selectDomain){
        if($selectNS == '' && $selectDomain == ''){
            $logInfo = $this->db->select("username,timestamp,log_type,log_contents,server_name,domain_name")
                ->from('logs')
                ->join('name_servers','name_servers.id = logs.id_server','left')
                ->join('dns_domains','dns_domains.id = logs.id_domain','left')
                ->group_start()
                ->like('server_name',$search)
                ->or_like('domain_name',$search)
                ->or_like('log_type',$search)
                ->or_like('log_contents',$search)
                ->group_end()
                ->limit($pageSize,((int)$page - 1) * $pageSize)
                ->order_by('timestamp','DESC')
                ->get()->result_array();

            $num = $this->db->select("username,timestamp,log_type,log_contents,server_name,domain_name")
                ->from('logs')
                ->join('name_servers','name_servers.id = logs.id_server','left')
                ->join('dns_domains','dns_domains.id = logs.id_domain','left')
                ->group_start()
                ->like('server_name',$search)
                ->or_like('domain_name',$search)
                ->or_like('log_type',$search)
                ->or_like('log_contents',$search)
                ->group_end()
                ->get()->num_rows();

            $log = [
                'logInfo' => $logInfo,
                'num'     => $num
            ];
        }else{
            if($selectNS == '' && $selectDomain != ''){
                $where = [
                    'dns_domains.id' => $selectDomain
                ];
            }else if($selectNS != '' && $selectDomain == ''){
                $where = [
                    'name_servers.id' => $selectNS
                ];
            }else{
                $where = [
                    'dns_domains.id' => $selectNS,
                    'name_servers.id' => $selectDomain
                ];
            }
            $logInfo = $this->db->select("username,timestamp,log_type,log_contents,server_name,domain_name")
                ->from('logs')
                ->join('name_servers','name_servers.id = logs.id_server','left')
                ->join('dns_domains','dns_domains.id = logs.id_domain','left')
                ->where($where)
                ->group_start()
                ->like('server_name',$search)
                ->or_like('domain_name',$search)
                ->or_like('log_type',$search)
                ->or_like('log_contents',$search)
                ->group_end()
                ->limit($pageSize,((int)$page - 1) * $pageSize)
                ->order_by('timestamp','DESC')
                ->get()->result_array();

            $num = $this->db->select("username,timestamp,log_type,log_contents,server_name,domain_name")
                ->from('logs')
                ->join('name_servers','name_servers.id = logs.id_server','left')
                ->join('dns_domains','dns_domains.id = logs.id_domain','left')
                ->where($where)
                ->group_start()
                ->like('server_name',$search)
                ->or_like('domain_name',$search)
                ->or_like('log_type',$search)
                ->or_like('log_contents',$search)
                ->group_end()
                ->get()->num_rows();

            $log = [
                'logInfo' => $logInfo,
                'num'     => $num
            ];
        }

        return $log;
    }

    /**
     * @return mixed
     * 获取name_server
     */
    public function getNS(){
        $res = $this->db->select('id,server_name')->from('name_servers')
            ->order_by('server_name')
            ->get()->result_array();
        return $res;
    }

    /**
     * @return mixed
     * 获取 domain列表
     */
    public function getDomainSelect(){
        $res = $this->db->select('id,domain_name')->from('dns_domains')
            ->order_by('domain_name')
            ->get()->result_array();
        return $res;
    }

}
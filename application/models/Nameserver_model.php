<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/31
 * Time: 14:23
 * 域名服务器的操作
 */
class Nameserver_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param $data
     * @return bool
     * 添加名称服务器
     */
    public function addData($data)
    {
        //判断是否设置为主服务器
        if($data['server_primary'] == 1)
        {
            //查看是否已有主服务器
            $rows = $this->db->select()->from('name_servers')
                ->where('server_primary',1)
                ->get()
                ->result_array();
            if($rows)//如果有
            {
                //修改server_primary = 0
                $this->db->update('name_servers',array('server_primary'=>0),'id ='.$rows[0]['id']);
            }
        }
        $this->db->trans_begin();
        $this->db->insert('name_servers',$data);
        //判断是否用作公共的ns记录
        if($data['server_record'] == 1)
        {
            //查询所有域
            $domains = $this->db->select('id,domain_name')->from('dns_domains')
                ->get()
                ->result_array();
            if(!empty($domains))
            {
                $this->_dealNameServer($domains);
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }

    /**
     * @param $domains
     * @return bool
     * 处理nameServer添加修改删除
     */
    public function _dealNameServer($domains)
    {
        foreach($domains as $v)
        {
            //删除dns_records对应域的ns记录
            $id_domain = $v['id'];
            $where = "id_domain = $id_domain and type = 'NS'";
            $this->db->where($where);
            $this->db->delete('dns_records');
            //查询域的组
            $group = $this->db->select('id_group')->from('dns_domains_groups')
                ->where('id_domain',$id_domain)
                ->get()
                ->result_array();
            foreach($group as $gr)
            {
                $group_arr[] = $gr['id_group'];
            }
            $group_str = implode(',',$group_arr);
            $where = "id_group in(".$group_str.") and server_record =1";
            $name_servers = $this->db->select('server_name')->from('name_servers')
                ->where($where)
                ->get()
                ->result_array();
            if(!empty($name_servers))
            {
                foreach($name_servers as $val)
                {
                    ////添加ns记录
                    $this->db->insert('dns_records',
                        [
                            'id_domain' => $v['id'],
                            'name' => $v['domain_name'],
                            'type' => 'NS',
                            'content' => $val['server_name'],
                            'ttl' => 86400,
                            'prio' => 0
                        ]
                    );
                }
            }
        }
    }
    /**
     * @param $page
     * @param $pagesize
     * @return array
     * 分页显示列表
     */
    public function getServerList($page,$pagesize)
    {
        $rows = $this->db->select('name_servers_groups.group_name,name_servers.*')->from('name_servers')
            ->join('name_servers_groups','name_servers.id_group = name_servers_groups.id')
            ->limit($pagesize,($page-1)*$pagesize)
            ->get()
            ->result_array();
        $num = $this->db->select('name_servers_groups.group_name,name_servers.*')->from('name_servers')
            ->join('name_servers_groups','name_servers.id_group = name_servers_groups.id')
            ->get()
            ->num_rows();
        return [
            'num'=> $num,
            'serverList' => $rows
        ];
    }
    /**
     * @param $id
     * @return array
     */
    public function getIdData($id)
    {
        $rows = $this->db->select()->from('name_servers')
            ->where('id',$id)
            ->get()
            ->result_array();
        if($rows)
        {
            return $rows[0];
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     * 根据主键修改数据
     */
    public function updatIdDate($data)
    {
        //判断是否设置为主服务器
        if($data['server_primary'] == 1)
        {
            //查看是否已有主服务器
            $rows = $this->db->select()->from('name_servers')
                ->where('server_primary',1)
                ->get()
                ->result_array();
            if($rows)//如果有
            {
                //修改server_primary = 0
                $this->db->update('name_servers',array('server_primary'=>0),'id ='.$rows[0]['id']);
            }
        }
        $this->db->trans_begin();
        $this->db->replace('name_servers',$data);
        //查询所有域
        $domains = $this->db->select('id,domain_name')->from('dns_domains')
            ->get()
            ->result_array();
        if(!empty($domains))
        {
            $this->_dealNameServer($domains);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }

    }

    /**
     * @param $id
     * @return bool
     * 根据主键删除数据
     */
    public function delIdData($id)
    {

        $this->db->trans_begin();
        //查看删除的nameServer server_record是否为1
        $data = $this->db->select()->from('name_servers')
            ->where('id',$id)
            ->get()->result_array();
        $this->db->delete('name_servers',array('id'=>$id));
        //判断是否用作公共的ns记录
        if($data[0]['server_record'] == 1)
        {
            //查询所有域
            $domains = $this->db->select('id,domain_name')->from('dns_domains')
                ->get()
                ->result_array();
            if(!empty($domains))
            {
                $this->_dealNameServer($domains);
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }
    /**\
     * @return array
     * 获取组列表
     */
    public function getGroupList()
    {
        $rows = $this->db->select()->from('name_servers_groups')
            ->get()
            ->result_array();
        return $rows;
    }
    /**
     * @param string $where
     * @return int $num
     */
    public function getWhereData($where = '')
    {
        if($where == '')
        {
            $where = ' 1';
        }
        $num = $this->db->select()->from('name_servers')
            ->where($where)
            ->get()
            ->num_rows();
        return $num;
    }
}
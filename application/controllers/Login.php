<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/9/4
 * Time: 下午5:35
 */

class Login extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    //加载登录页面
    public function showLogin(){
        $this->load->view('login.html');
    }

    public function login(){
        $username = $this->input->get('username');
        $password = $this->input->get('password');
        $res = $this->login_model->checkLogin($username,$password);
        echo json_encode($res);
    }

    public function logout(){
        unset($_SESSION['uid']);
        header("Location:/login/showLogin");
    }
}
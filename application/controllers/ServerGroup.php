<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/4
 * Time: 14:05
 */
class ServerGroup extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @param int $code
     * @param string $msg
     * 返回json数据
     */
    public function reJson($code,$msg)
    {
        $data = array(
            'code' => $code,
            'msg'  => $msg
        );
        echo json_encode($data);
        die();
    }
    //加载添加页面
    public function addGroup()
    {
        $this->display('serverGroup/add_group.html');
    }
    //接口:添加数据
    public function addData()
    {
        $data = $this->input->post();
        $addData = array(
            'group_name' => $data['group_name'],//组名
            'group_description' => $data['group_description']//描述
        );
        $this->load->model('Servergroup_model','group');
        $result = $this->group->add($addData);
        if($result === false)
        {
            $this->reJson(0,'添加服务器失败,请重试!');
        }else
        {
            $this->reJson(1,'添加服务器成功!');
        }
    }

    //加载服务器组列表页面
    public function groupList()
    {
        $this->display('serversGroup/group_list.html');
    }
    //接口:获取服务器组数据
    public function getGroupList()
    {
        $page = $this->input->get('page');
        $pagesize = $this->input->get('pagesize');
        $this->load->model('Servergroup_model','group');
        $rows = $this->group->getGroupList($page,$pagesize);
        $data = $this->groupMember();
        if($rows)
        {
            foreach($rows['group'] as $k =>$r)
            {
                $this->load->model('nameserver_model','nameServer');
                $num = $this->nameServer->getWhereData('id_group = '.$r['id']);
                if($num  >0)
                {
                    $rows['group'][$k]['group_member'] =$data[$r['id']];
                }else
                {
                    $rows['group'][$k]['group_member'] = '暂无成员';
                }

            }
        }
        echo json_encode($rows);
    }
    //加载详情页面/与修改页面
    public function groupDetails()
    {
        $this->display('serversGroup/group_details.html');
    }
    //接口:根据id获取组的详细信息
    public function getGroupDetails()
    {
        $id = $this->input->get('id');
        $this->load->model('Servergroup_model','group');
        $row = $this->group->getIdData($id);
        echo json_encode($row);
    }

    //接口:根据主键修改数据
    public function updateData()
    {
        $data = $this->input->post();
        $update = array(
            'id' => $data['id'],
            'group_name' => $data['group_name'],
            'group_description' => $data['group_description']
        );
        $this->load->model('Servergroup_model','group');
        $result = $this->group->update($update);
        if($result === false)
        {
            $this->reJson(0,'修改数据失败,请重试!');
        }else
        {
            $this->reJson(1,'修改数据成功!');
        }
    }
    //接口:根据主键删除数据
    public function delGroup()
    {
        $id = $this->input->get('id');
        $this->load->model('Servergroup_model','group');
        $result = $this->group->del($id);
        if($result == '0')
        {
            $this->reJson(0,'删除失败,请重试!');
        }elseif($result == '2')
        {
            $this->reJson(0,'删除失败,该组下面还有成员,请先删除成员信息!');
        }else
        {
            $this->reJson(1,'删除成功!');
        }
    }
    public function groupMember()
    {
        $this->load->model('Servergroup_model','group');
        $data = $this->group->getGroupMember();
        return $data;
    }
}
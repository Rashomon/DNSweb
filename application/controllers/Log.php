<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/9/4
 * Time: 下午2:11
 * 操作日志 控制器
 */
class Log extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('log_model');
    }

    //加载操作日志界面
    public function showLog(){
        $this->display('log/show_log.html');
    }

    //接口：分页获取操作日志数据
    public function getLogPage(){
        $page = $this->input->post('page');
        $pageSize = $this->input->post('pageSize');
        $search   = $this->input->post('search');
        $selectNS = $this->input->post('nameServer');
        $selectDomain = $this->input->post('domain');
        $logInfo  = $this->log_model->getLogInfo($page,$pageSize,$search,$selectNS,$selectDomain);
        echo json_encode($logInfo);
    }

    //接口：获取server_name下拉菜单
    public function getNSselect(){
        $data = $this->log_model->getNS();
        echo json_encode($data);
    }

    //接口：获取domain列表
    public function getDomainSelect(){
        $data = $this->log_model->getDomainSelect();
        echo json_encode($data);
    }
}
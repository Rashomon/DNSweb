<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/9/4
 * Time: 上午10:35
 * 配置控制器
 */

class Config extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('config_model');
    }

    //加载 配置页面
    public function showConfig(){
        $this->display("config/show_config.html");
    }

    //接口：获取当前配置文件信息
    public function getNowConfig(){
        $config = $this->config_model->getConfig();
        echo json_encode($config);
    }

    //接口：更新配置
    public function updateConfig(){
        $config = $this->input->post('config');
        $res = $this->config_model->updateConfig($config);
        echo json_encode($res);
    }

}
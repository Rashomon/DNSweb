<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/8/30
 * Time: 下午2:32
 * NameServer管理控制器
 */

class NameServers extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @param int $code
     * @param string $msg
     * 返回json数据
     */
    public function reJson($code,$msg)
    {
        $data = array(
            'code' => $code,
            'msg'  => $msg
        );
        echo json_encode($data);
        die();
    }
    //加载:域名服务器添加页面
    public function addServer()
    {
        $this->display('nameServers/add_servers.html');
    }
    /**
     * 接口:添加数据
     */
    public function addData()
    {
        $data = $this->input->post();
        $addData = array(
            'id_group'=>$data['id_group'],//服务器组
            'server_primary' => $data['server_primary'],//0未选中 1选中
            'server_record'=> $data['server_record'],//0未选中 1选中
            'server_name' => $data['server_name'],//服务器名称
            'server_description'=> $data['server_description'],//描述
            'server_type' => $data['server_type'],//服务器类型
            'api_auth_key' => $data['api_auth_key'],//api秘钥
            'api_sync_config'=> 1,
            'api_sync_log' => 1,
        );
        $this->load->model('nameserver_model','nameServer');
        $result = $this->nameServer->addData($addData);
        if($result === false)
        {
            $this->reJson(0,'添加域名成失败,请稍后重试!');
        }else{
            $this->reJson(1,'添加域名成功');
        }
    }
    //加载服务器列表页面
    public function serverList()
    {
        $this->display('nameServers/servers_list.html');
    }
    /**
     * 接口:获取域名服务器列表数据
     */
    public function getServerList()
    {
        $page = $this->input->get('page');
        $pagesize = $this->input->get('pagesize');
        $this->load->model('nameserver_model','nameServer');
        $rows = $this->nameServer->getServerList($page,$pagesize);
        echo json_encode($rows);
    }
    //加载详情页面/修改回显页面
    public function serverDetails()
    {
        $this->display('nameServers/server_details.html');
    }
    /**
     * 接口:根据id获取数据详情
     */
    public function editShow()
    {
        $id = $this->input->get('id');
        $this->load->model('nameserver_model','nameServer');
        $row = $this->nameServer->getIdData($id);
        echo json_encode($row);
    }
    /**
     * 接口:更新数据
     */
    public function updateData()
    {
        $data = $this->input->post();
        $updateData = array(
            'id' => $data['id'],
            'id_group'=>$data['id_group'],//服务器组
            'server_primary' => $data['server_primary'],
            'server_record'=> $data['server_record'],
            'server_name' => $data['server_name'],//服务器名称
            'server_description'=> $data['server_description'],//描述
            'server_type' => $data['server_type'],//服务器类型
            'api_auth_key' => $data['api_auth_key'],//api秘钥
            'api_sync_config'=> 1,
            'api_sync_log' => 1,
        );
        $this->load->model('nameserver_model','nameServer');
        $result = $this->nameServer->updatIdDate($updateData);
        if($result === false)
        {
            $this->reJson(0,'修改服务器名称失败,请稍后重试!');
        }else
        {
            $this->reJson(1,'修改服务器名称成功!');
        }
    }
    /**
     * 接口:根据主键id删除数据
     */
    public function delServer()
    {
        $id = $this->input->get('id');
        $this->load->model('nameserver_model','nameServer');
        $result = $this->nameServer->delIdData($id);
        if($result === false )
        {
            $this->reJson(0,'删除域名失败,请重试!');
        }else
        {
            $this->reJson(1,'删除域名成功!');
        }
    }
    //获取组列表 接口
    public function getGroupList()
    {
        $this->load->model('nameserver_model','nameServer');
        $re = $this->nameServer->getGroupList();
        echo json_encode($re);
    }
}
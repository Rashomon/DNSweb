<?php
/**
 * Created by PhpStorm.
 * User: maskerj
 * Date: 2017/8/30
 * Time: 下午2:34
 * 用户模块管理控制器
 */
class UserManage extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    //加载用户列表页面
    public function showUserList(){
        $this->display("user/user_list.html");
    }

    //接口：获取用户列表信息
    public function getUserList(){
        $searchData = $this->input->post('search');
        $page       = $this->input->post('page');
        $pageSize   = $this->input->post('pageSize');
        $sort       = $this->input->post('sort');
        $sortKey    = $this->input->post('sortKey');
        $userList = $this->user_model->getUserList($searchData,$page,$pageSize,$sort,$sortKey);
        echo json_encode($userList);
    }

    //加载 创建用户页面
    public function showAddUser(){
        $this->display("user/add_user.html");
    }

    //接口：添加用户
    public function addUser(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $realname = $this->input->post('realname');
        $email    = $this->input->post('email');
        $res = $this->user_model->addUser($username,$password,$realname,$email);
        echo json_encode($res);
    }

    //加载 用户详情页面
    public function showUserDetail(){
        $this->display("user/user_detail.html");
    }

    //接口：获取用户详情信息
    public function getUserInfo(){
        $uid = $this->input->get('uid');
        $userInfo = $this->user_model->getUserInfo($uid);
        echo json_encode($userInfo);
    }


    //接口：更新用户详情
    public function updateUserInfo(){
        $data = $this->input->post('data');
        $res  = $this->user_model->updateUser($data);
        echo json_encode($res);
    }

    //加载 用户权限页面
    public function showUserPermission(){
        $this->display("user/user_permission.html");
    }

    //接口:获取用户权限
    public function getUserPer(){
        $uid = $this->input->get('uid');
        $userPer = $this->user_model->getUserPer($uid);
        echo json_encode($userPer);
    }

    //接口：更新用户权限
    public function updateUserPer(){
        $per = $this->input->post('data');
        $uid = $this->input->post('uid');
        $res = $this->user_model->updateUserPer($per,$uid);
        echo json_encode($res);
    }

    //接口:删除用户
    public function deleteUser(){
        $uid = $this->input->get('uid');
        $res = $this->user_model->delUser($uid);
        echo json_encode($res);
    }


}
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="/js/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="/js/layui/css/layui.css" />
    <link rel="stylesheet" type="text/css" href="/css/layout.css"/>
    <link href='/img/logo.png' rel='icon'>
    <{block name='css'}><{/block}>
    <!-- jquery js -->
    <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
    <!-- bootstrap.js -->
    <script type="text/javascript" src="/js/bootstrap/bootstrap.min.js"></script>
    <!-- layui.js -->
    <script type="text/javascript" src="/js/layer/layer.js"></script>
    <script type="text/javascript" src="/js/layui/layui.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <!-- title -->
    <title>立普威陆IPv6域名解析系统</title>
</head>
<body>
<{include file="header.tpl"}>
<{include file="nav.tpl"}>
<main class="m-main-box">
    <section class="m-main-content">
        <{block name=content}><{/block}>
    </section>
</main>
<{block name=js}><{/block}>
</body>
</html>
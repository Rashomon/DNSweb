<nav class="v6-m-nav">
    <ul class="v6-u-first-nav">
        <li>
            <a class="u-first-level" href="/log/showLog">
                <i class="glyphicon glyphicon-home"></i>
                更新日志
            </a>
        </li>
        <li><a class="u-first-level" href="/config/showConfig"><i class="glyphicon glyphicon-stats"></i>配置文件信息</a></li>
        <li>
            <a class="u-first-level" href="javascript:void(0)">
                <i class="glyphicon glyphicon-user"></i>用户信息
                <span class="glyphicon glyphicon-chevron-down"></span>
            </a>
            <ul class="v6-u-secend-nav">
                <li><a class="u-second-level" href="/UserManage/showUserList"">用户列表</a></li>
                <li><a class="u-second-level" href="/UserManage/showAddUser">添加用户</a></li>
            </ul>
        </li>
        <li>
            <a class="u-first-level" href="javascript:void(0)">
                <i class="glyphicon glyphicon-list-alt"></i>服务器名称信息
                <span class="glyphicon glyphicon-chevron-down"></span>
            </a>
            <ul class="v6-u-secend-nav">
                <li><a class="u-second-level" href="/NameServers/serverList">服务器列表</a></li>
                <li><a class="u-second-level" href="/NameServers/addServer">添加服务器</a></li>
                <li><a class="u-second-level" href="/serverGroup/groupList">管理服务器组</a></li>
            </ul>
        </li>
        <li>
            <a class="u-first-level" href="javascript:void(0)">
                <i class="glyphicon glyphicon-list"></i>域名/区域管理
                <span class="glyphicon glyphicon-chevron-down"></span>
            </a>
            <ul class="v6-u-secend-nav">
                <li><a class="u-second-level" href="/Domains/dnsList">查看域列表</a></li>
                <li><a class="u-second-level" href="/Domains/addDomains">添加域</a></li>
            </ul>
        </li>
        <li>
            <a class="u-first-level" href="/Login/logout">
                <i class="glyphicon glyphicon-log-out"></i>
                退出
            </a>
        </li>
    </ul>
</nav>
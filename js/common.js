/********** nav events start *************/
$(function(){
    var speed = 300; //动画过度时间
    showMenuURL();
    // 一级菜单点击事件
    $('body').delegate('.u-first-level','click',function(){
        if($(this).parent().hasClass('active')){
            $(this).parent().find('.v6-u-secend-nav').slideUp(speed);
            $(this).parent().removeClass('active');
            $(this).find('span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down')
        }else{
            $(this).parent().find('.v6-u-secend-nav').slideDown(speed);
            $(this).parent().addClass('active');
            $(this).find('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            if($(this).parent().siblings('.active')){
                var targ = $(this).parent().siblings('.active');
                targ.find('.u-first-level span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down')
                targ.find('.v6-u-secend-nav').slideUp(speed);
                targ.removeClass('active');
            }
        }
        $('.v6-u-third-nav').hide();
    })

    // 二级菜单点击事件
    $('body').delegate('.u-second-level','click',function(){
        if($(this).parent().hasClass('active')){
            $(this).parent().find('ul').slideUp(speed);
            $(this).parent().removeClass('active');
        }else{
            $(this).parent().find('ul').slideDown(speed);
            $(this).parent().addClass('active');
        }
    })
})
/********** nav events end *************/

function ajaxObj(url, type, data, fn, async){
    var layer2 = layer.load(2);
    async = async || false;
    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'json',
        success: function(obj){
            layer.close(layer2);
            fn(obj);
        },
        errorFn: function(err){
            layer.close(layer2);
            layer.msg('网络请求失败,请检查网络配置！');
        }
    })
}

function getLocalTime(nS) {
    var now = new Date(parseInt(nS) * 1000);
    var yy = now.getFullYear();      //年
    var mm = now.getMonth() + 1;     //月
    var dd = now.getDate();          //日
    var hh = now.getHours();         //时
    var ii = now.getMinutes();       //分
    var ss = now.getSeconds();       //秒
    var dateTime = yy + "-";
    if (mm < 10) dateTime += "0";
    dateTime += mm + "-";
    if (dd < 10) dateTime += "0";
    dateTime += dd + " ";
    if (hh < 10) dateTime += "0";
    dateTime += hh + ":";
    if (ii < 10) dateTime += '0';
    dateTime += ii + ":";
    if (ss < 10) dateTime += '0';
    dateTime += ss;
    return dateTime;
    //return new Date(parseInt(nS) * 1000).toLocaleString().replace(/年|月/g, "-").replace(/日/g, " ");
}

function showMenuURL(){
    if($('.u-nav-url').length){
        var href = $('.u-nav-url').attr('href');
        var targ = $('.v6-u-secend-nav a[href="' +  href + '"]');
        var targUl = targ.parent().parent();
        targ.parent().addClass('active');
        targUl.show();
        targUl.parent().addClass('active');
    }else{
        var url = window.location.pathname;
        var search = window.location.search;

        var targNav = $('a[href="' + url + search + '"]');
        if (targNav) {
            var parent = targNav.parent().parent();

            if (parent.hasClass('v6-u-first-nav')) {    //一级菜单
                targNav.parent().addClass('active');
            } else if (parent.hasClass('v6-u-secend-nav')) {    //二级菜单
                parent.show();
                parent.parent().addClass('active');
                targNav.parent().addClass('active');
            }
        }
    }
}


//将序列化字符串装换为对象
function paramString2obj (serializedParams) {    
    var obj={};
    function evalThem (str) {
        var strAry = new Array();
        strAry = str.split("=");
        //使用decodeURIComponent解析uri 组件编码
        for(var i = 0; i < strAry.length; i++){
            strAry[i] = decodeURIComponent(strAry[i]);
        }
        var attributeName = strAry[0];
        var attributeValue = strAry[1].trim();
        //如果值中包含"="符号，需要合并值
        if(strAry.length > 2){
            for(var i = 2;i<strAry.length;i++){
                attributeValue += "="+strAry[i].trim();
            }
        }
        if(!attributeValue){
            return ;
        }
        
        var attriNames = attributeName.split("."),
            curObj = obj;
        for(var i = 0; i < (attriNames.length - 1); i++){
            curObj[attriNames[i]]?"":(curObj[attriNames[i]] = {});
            curObj = curObj[attriNames[i]];
        }
        
        //使用赋值方式obj[attributeName] = attributeValue.trim();替换
        //eval("obj."+attributeName+"=\""+attributeValue.trim()+"\";");
        //解决值attributeValue中包含单引号、双引号时无法处理的问题
        //这里可能存在一个种情况：多个checkbox同一个name的时候需要使用","来分割 
        curObj[attriNames[i]] = curObj[attriNames[i]]? 
        (curObj[attriNames[i]] + "," + attributeValue.trim()): 
        attributeValue.trim();
    };
    
    var properties = serializedParams.split("&");
    for (var i = 0; i < properties.length; i++) {
        //处理每一个键值对
        evalThem(properties[i]);
    };
    return obj;
}